import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent {
  URL = 'https://cdn.iconscout.com/icon/premium/png-256-thumb/delete-1432400-1211078.png'
  @Input() toDo: string = '';
  @Output() delete = new EventEmitter();
  @Output() changeToDo = new EventEmitter();
  showBorder = '';
  constructor() { }
  onFocus(){
    this.showBorder = 'readyFocus';

  }
  outFocus(){
    this.showBorder = '';
  }
  changeToDoInput(event:Event) {
    const target = <HTMLInputElement>event.target;
    this.changeToDo.emit(target.value)
  }
  deleteInArray(){
    this.delete.emit(this.toDo);
  }

}
