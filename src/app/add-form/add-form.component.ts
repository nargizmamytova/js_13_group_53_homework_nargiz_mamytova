import {Component, EventEmitter, Input, Output} from '@angular/core';
import {query, style} from "@angular/animations";

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent {
  toDoArray: string[] = [];
  showToDo = false;
  toDo = '';
  URL = 'https://cdn.iconscout.com/icon/premium/png-256-thumb/delete-1432400-1211078.png'

  constructor() {
  }

  onInput(event: Event) {
    event.preventDefault();
    this.showToDo = true;
    this.toDoArray.push(this.toDo);
    this.reset()
  }
  reset(){
    this.toDo = '';
  }
  onDelete(index: number) {
    this.toDoArray.splice(index, 1);
  }
  changeInput(index: number, newToDo: string){
    this.toDoArray[index] = newToDo;
  }
}


